<?php

/**
 * @file
 * Contains the migration class.
 */

/**
 * Base class for incremental source migrations.
 *
 * Important things to note:
 *  - This only works with sources that use uploadable files, such as csv.
 *  - The mapping must use an ID field that is unique across all source files.
 *    In particular, if using the MigrateSourceCSV source, do not use the
 *    csvrownum field as the mapping ID, since that will repeat in each file.
 */
abstract class MigrateIncrementalSourceMigration extends Migration {

  /**
   * The array of UIRs for the source files this migration uses.
   */
  protected $source_files = array();

  /**
   * The URI of the current source file.
   *
   * This should be used when defining the migration's source plugin.
   */
  protected $current_source_file;

  /**
   * Migration constructor.
   *
   * Additional properties in the arguments:
   *  - 'current_source_file': The URI for the current source file to run the
   *    migration for. This will be in the private:// file directory.
   *  - 'source_files': An array of all source files that this migration uses,
   *    as URIs. This should include the current source file.
   */
  public function __construct($arguments) {
    parent::__construct($arguments);

    $arguments += array(
      'current_source_file' => NULL,
      'source_files' => array(),
    );

    // Store our CSV file data so that these can be retrieved and changed by the
    // wizard.
    $this->source_files = $arguments['source_files'];
    $this->current_source_file = $arguments['current_source_file'];
  }

  /**
   * Accessor for the wizard to get the source files, so it can add to them.
   */
  public function getSourceFiles() {
    return $this->source_files;
  }

  /**
   * Helper for the wizard to determine if the current source has run.
   *
   * @return
   *  A boolean indicating whether the migration has fully processed its latest
   *  source.
   */
  public function isFullyProcessed() {
    // HUH? This doesn't exist in the migration class, only in the UI!
    // See https://drupal.org/node/2231491
    $source_count = $this->sourceCount();
    $processed_count = $this->processedCount();

    // Processed count could be LARGER than the source count, as this
    // migration may have already run through multiple sources. However, if
    // it's less than the source count then this source definitely hasn't
    // been imported.
    if ($processed_count < $source_count) {
      return FALSE;
    }
    else {
      return TRUE;
    }
  }

}
