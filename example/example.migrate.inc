<?php

/**
 * @file
 * Contains example code.
 *
 * Use the dummy source CSVs in the subfolder as files to upload.
 */

/**
 * Implements hook_migrate_api().
 */
function mymodule_migrate_api() {
  $api = array(
    'api' => 2,
    // Register your wizard class.
    'wizard classes' => array(
      'MigrateIncrementalSourceDemoWizard',
    ),
    // There is no need to register the group or the migrations: the wizard
    // takes care of this.
    'groups' => array(
    ),
    'migrations' => array(
    ),
  );
  return $api;
}

/**
 * Sample wizard class.
 */
class MigrateIncrementalSourceDemoWizard extends MigrateIncrementalSourceWizard {

  /**
   * Our group name and title.
   */
  protected $groupName = 'IncrementalDemo';
  protected $groupTitle = 'Incremental Demo';

  /**
   * Identify ourselves.
   *
   * @return string
   */
  public function getSourceName() {
    return t('Demo incremental source wizard');
  }

  /**
   * Define our migrations.
   */
  protected $migration_info = array(
    // The key is immaterial, but helpful for readability.
    'MigrateIncrementalSourceDemo' => array(
      'machine_name' => 'MigrateIncrementalSourceDemo',
      'class_name' => 'MigrateIncrementalSourceDemoMigration',
      'arguments' => array(),
    ),
  );

  /**
   * Define the file extensions we allow.
   */
  protected $file_upload_extensions = 'csv';

}

/**
 * Demo migration class.
 */
class MigrateIncrementalSourceDemoMigration extends MigrateIncrementalSourceMigration {

  public function __construct($arguments) {
    parent::__construct($arguments);

    // Define how our mapping gets stored.
    $this->map = new MigrateSQLMap($this->machineName,
      array(
        // Do NOT use CSV row number, as we must use an ID for the map that is
        // unique across all source files.
        'id' => array(
          'type' => 'int',
          'not null' => TRUE,
          'description' => "Source ID",
        ),
      ),
      MigrateDestinationNode::getKeySchema()
    );

    // Define the CSV columns. Set them on the migration object so we can refer
    // to them in prepareRow().
    $this->csv_columns = array(
      0 => array('id', 'ID'),
      1 => array('title', 'Title'),
    );

    // This tells MigrateSourceCSV not to use the first row as CSV headers.
    $source_options = array('header_rows' => 0);

    // The source file property is a fully qualified URI to the file uploaded
    // to the private:// file directory.
    $this->source = new MigrateSourceCSV($this->current_source_file, $this->csv_columns, $source_options, array());

    // Set up our destination.
    $this->destination = new MigrateDestinationNode(
      // Bundle name.
      'article'
    );

    // Mappings.
    // Data coming from the CSV file.
    $this->addFieldMapping('title', 'title')
      ->description(t("The node title."));

  }

}
