<?php

/**
 * @file
 * Contains the wizard class.
 */

/**
 * Wizard base class.
 *
 * Works with migrations that are subclasses of
 * MigrateIncrementalSourceMigration.
 *
 * Subclasses should register themselves using hook_migrate_api().
 */
abstract class MigrateIncrementalSourceWizard extends MigrateUIWizard {

  /**
   * Returns the translatable name representing the source of the data (e.g.,
   * "Drupal", "WordPress", etc.).
   *
   * @return string
   */
  /*
  // This is here as a DX reminder for subclasses to implement this.
  abstract public function getSourceName();
  */

  /*
  protected $groupName = 'change this in your subclass';
  protected $groupTitle = 'This too';
  */

  /**
   * An array of data about migrations this wizard registers.
   *
   * Subclasses should set this. Migrations must be subclasses of
   * MigrateIncrementalSourceMigration.
   *
   * These do not need to be registered in hook_migrate_api(), as the wizard's
   * UI performs registration. (It is however possible to have the wizard add
   * sources to migrations which *are* registered with hook_migrate_api(), but
   * this requires special handling.)
   *
   * Each item in the array should be itself an array containing the following
   * keys:
   *  - 'machine_name': The machine name of the migration, without the group
   *    name prefix.
   *  - 'class_name': The class name of the migration.
   *  - 'arguments': (optional) An array of values to pass to
   *    Migration::registerMigration() and thus to the migration as construction
   *    arguments. It should be noted that this array corresponds to the array
   *    of data for each migration in hook_migrate_api(), apart from the
   *    'class_name' property. For example, a 'dependencies' property may be
   *    included here.
   */
  protected $migration_info = array();

  /**
   * Allowed file extensions for the upload.
   *
   * Subclasses should set this to match the source plugin their migrations use.
   *
   * A string space separated list of allowed extensions, same format as for
   * file_validate_extensions().
   */
  protected $file_upload_extensions = '';

  /**
   * Lay out the basic steps of the wizard.
   */
  public function __construct() {
    parent::__construct();

    // Keep an array of steps so subclasses can insert steps among them.
    // To add a step after the upload step, do:
    // addStep($name, $form_method, $this->steps['upload']).
    $this->steps = array();

    $this->steps['upload'] = $this->addStep(t('Source file upload'), 'sourceUploadForm');
    $this->steps['review'] = $this->addStep(t('Review'), 'reviewForm');
  }

  /**
   * Upload source file.
   */
  protected function sourceUploadForm(&$form_state) {
    $form = array();

    // Check private files are configured before we allow anthing.
    if (!variable_get('file_private_path', FALSE)) {
      form_set_error('', t('You must specify a private file system path in the <a href="!url">file system settings</a> to upload source files.', array(
        '!url' => url('/admin/config/media/file-system'),
      )));

      return $form;
    }

    // Check that our migrations have completely imported their latest source.
    foreach ($this->getMigrations() as $migration) {
      if (!$migration->isFullyProcessed()) {
        // Set an error message now, to save the user a pointless file upload.
        // This also prevents validation.
        form_set_error('', t('The most recently added source has not yet fully imported. You may not import a new source.'));

        // We only need to check that any one migration fails this.
        return $form;
      }
    }

    $form['source_file'] = array(
      '#title' => t('New source file'),
      '#type' => 'managed_file',
      '#description' => t('The source file to add to this migration.'),
      '#upload_location' => 'private://migrate_incremental/' . $this->groupName,
      '#upload_validators' => array("file_validate_extensions" => array($this->file_upload_extensions)),
    );

    return $form;
  }

  /**
   * Validation for the upload step.
   */
  protected function sourceUploadFormValidate(&$form_state) {
  }

  /**
   * Review step.
   */
  protected function reviewForm(&$form_state) {
    $form = array();

    // Store the source file permanently.
     // Load the file via file.fid.
    $file = file_load($form_state['values']['source_file']);
    // Change status to permanent.
    $file->status = FILE_STATUS_PERMANENT;
    // Save.
    file_save($file);
    // Record that the module (in this example, user module) is using the file.
    // Unfortunately the managed file system only works in numeric IDs, so we
    // can't record which group we work with, so just pass in a 0.
    file_usage_add($file, 'migrate_incremental_source_wizard', 'migration', 0);

    $message = t('Please review your migration settings. When you submit this
      form, a migration job containing a migration task for each type of item to be
      imported will be created and you will be left at the
      dashboard.');

    $form['description'] = array(
      '#prefix' => '<div>',
      '#markup' => $message,
      '#suffix' => '</div>',
    );

    $form['filename'] = array(
      '#prefix' => '<div><strong>',
      '#markup' => t('New source file: ') . $file->filename,
      '#suffix' => '</strong></div>',
    );

    // Get the new source file uri from the file object.
    $new_source_file = $file->uri;

    // We either register or re-register all our migrations.
    // Get the migrations, if they already exist.
    $migrations = $this->getMigrations();
    foreach ($this->migration_info as $migration_info) {
      // Get the migration arguments from the settings.
      $migration_arguments = isset($migration_info['arguments']) ? $migration_info['arguments'] : array();

      // Either all the migrations exist, or none do: we only need to check the
      // array is non-empty.
      if ($migrations) {
        $migration = $migrations[$migration_info['machine_name']];

        // Get the existing source files list from the migration as it is
        // currently defined.
        $source_files = $migration->getSourceFiles();
      }
      else {
        $source_files = array();
      }

      // We need to set the new source as current, and also add it to the
      // complete sources array.
      $source_files[] = $new_source_file;

      // Add the source data to the migration arguments.
      // Note that we have to do this here rather than in the migration class
      // because addMigration() and registerMigration() don't instantiate a
      // migration, but just save data to the {migrate_status} table.
      $migration_arguments['current_source_file'] = $new_source_file;
      $migration_arguments['source_files'] = $source_files;

      $this->addMigration($migration_info['machine_name'], $migration_info['class_name'], $migration_arguments);
    }

    return $form;
  }

  /**
   * Get the migrations for this wizard, if they are already registered.
   *
   * @return
   *  An array of migration objects, keyed by machine names that are set in
   *  $migration_info. If they are not yet registered, an empty array.
   */
  function getMigrations() {
    $migrations = array();
    foreach ($this->migration_info as $migration_info) {
      $migration = $this->getMigration($migration_info['machine_name']);
      if ($migration) {
        $migrations[$migration_info['machine_name']] = $migration;
      }
    }

    return $migrations;
  }

  /**
   * Get the migration if it's already registered.
   *
   * We can't use Migration::getInstance as that outputs an error message if the
   * migration doesn't exist.
   *
   * @param $migration_machine_name
   *  The machine name of the migration, without the group name prefix.
   *
   * @return
   *  The migration object, or NULL if it's not yet registered.
   */
  function getMigration($migration_machine_name) {
    // Add the group name prefix. I am still not exactly sure why
    // MigrateUIWizard::addMigration() does this!
    $migration_machine_name = $this->groupName . $migration_machine_name;

    $row = db_select('migrate_status', 'ms')
           ->fields('ms', array('class_name', 'group_name', 'arguments'))
           ->condition('machine_name', $migration_machine_name)
           ->execute()
           ->fetchObject();

    if ($row) {
      $migration = Migration::getInstance($migration_machine_name);
      return $migration;
    }
    else {
      return NULL;
    }
  }

}
