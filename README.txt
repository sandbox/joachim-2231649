Migrate Incremental Source Wizard
=================================

This module provides base classes for creating Migrate UI wizards that let you
incrementally add source files to a migration.

The intention is that you have this workflow:

- User uploads a source CSV file
- User runs the migration
- User uploads a further source CSV file with newer data
- User runs the migration

Requirements:

- The source files must have a source ID that is unique across all of them, for
  the migration map to make use of.

Implementation
--------------

- Create a subclass of the wizard class for your own wizard. This should declare
  the migration(s) it uses among other thing: see the class documentation.
- Create one or more migrations. These should subclass the migration in this
  module so that they work with the wizard.

See the example folder for an annotated example.

Caveats
-------

The Migrate UI will not report the source as fully imported, because it will
find that there are MORE imported records that source records. Hence the UI
will say 'Import incomplete, not currently running', even though everything from
the current source is imported.
See https://drupal.org/node/2231491